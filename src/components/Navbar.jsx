import React from "react";
import "../styles/Navbar.css";

function Navbar() {
  return (
    <div className="navbar">
      <span>TryYourLuck</span>
    </div>
  );
}

export default Navbar;
