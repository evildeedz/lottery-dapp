import React, { useState } from "react";
import "../styles/ConnectWallet.css";
import ConnectWalletModal from "./ConnectWalletModal";

function ConnectWallet() {
  const [clicked, setClicked] = useState(false);

  return (
    <div className="connect-wallet To Begin">
      {clicked && <ConnectWalletModal setClicked={setClicked} />}
      <button onClick={() => setClicked(true)}>Connect Wallet To Begin</button>
    </div>
  );
}

export default ConnectWallet;
