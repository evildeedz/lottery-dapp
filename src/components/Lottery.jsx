import React, { useContext, useEffect, useState } from "react";
import { toastContext } from "../App";
import "../styles/Lottery.css";
import { useWeb3React } from "@web3-react/core";
import abi from "../lottery-erc20.json";
import { ethers } from "ethers";

function Lottery() {
  const { errorToast, successToast } = useContext(toastContext);
  const { active, chainId, account, error, connector, activate, deactivate, library } = useWeb3React();
  const [contract, setContract] = useState();
  const [balance, setBalance] = useState("0");
  const [lotteryId, setLotteryId] = useState(0);
  const [players, setPlayers] = useState([]);
  const [amount, setAmount] = useState("");
  const [winnersList, setWinnersList] = useState([]);

  // Setting Contract on Library
  useEffect(() => {
    if (library !== undefined && contract === undefined) setContract(new ethers.Contract("0xDE7107bcf94c2BC9ec7515B7781C5521d54211Ad", abi, library.getSigner()));
  }, [library]);

  // Get Money Entered Periodically When We Get a Contract
  useEffect(() => {
    if (contract !== undefined) {
      getContractStates();
      setInterval(() => {
        getContractStates();
      }, 1000 * 5);
    }
  }, [contract]);

  // Get Contract & Setting Its States
  async function getContractStates() {
    setBalance(ethers.utils.formatEther(await contract.getBalance()));
    const lottr = (await contract.lotteryId()).toNumber();
    setLotteryId(lottr);
    setPlayers(await contract.getPlayers());
    var winnerArr = [];
    for (var i = lottr - 1; i > 0; i--) {
      winnerArr.push(await contract.getWinnerByLottery(i));
    }
    setWinnersList(winnerArr);
  }

  // Pick Winner
  async function pickWinner() {
    if (players.length > 1) {
      try {
        await contract.pickWinner({ gasLimit: 3000000 });
      } catch (err) {
        console.log(err);
      }
    } else {
      errorToast("Not enough players.");
    }
  }

  // On Submit
  async function onSubmit(e) {
    e.preventDefault();
    const amountBigNumber = ethers.utils.parseEther(amount.toString());
    const minValue = ethers.utils.parseEther("0.01");
    // Checking if Amount is greater than 0.01 ETH
    if (amountBigNumber.gt(minValue)) {
      // Checking if enough balance
      if (amountBigNumber.lte(await library.getBalance(account))) {
        await contract.enter({ gasLimit: 3000000, value: amountBigNumber });
        successToast("Transaction in progress");
        setAmount("");
      } else {
        errorToast("You do not have sufficient funds");
      }
    } else {
      errorToast("Amount entered must be greater than 0.01 ETH");
    }
  }

  return (
    <div className="lottery">
      <div className="lottery-content">
        <div className="lottery-pool">
          <h1>Lottery Pool</h1>
          <h2>{balance} SepoliaETH</h2>
          <hr />
        </div>
        <div className="lottery-main">
          <div className="enter-eth">
            <form onSubmit={onSubmit}>
              <h1>Enter Lottery</h1>
              <input type="number" step="any" onChange={(e) => setAmount(e.target.value)} value={amount} />
              <br />
              <button type="submit">Submit</button>
            </form>
            <hr />
          </div>
          <div className="previous-winners">
            <h1>Previous Winners</h1>
            <div className="winner-list">
              {winnersList.map((element, index) => (
                <div className="winner" key={index}>
                  <h3>{element.slice(0, 10) + "...." + element.slice(-10)}</h3>
                  {winnersList.length - 1 !== index && <hr />}
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="pick-winner">
          <hr />
          <h4>(Only the contract deployer can use this button. As this is a project showcase, please contact EviLDeEdZ to execute pick winner. You can also fork the project, and deploy the SC found in project repository to test the winner picking functionality.)</h4>
          <button onClick={pickWinner}>Pick Winner</button>
        </div>
      </div>
    </div>
  );
}

export default Lottery;
