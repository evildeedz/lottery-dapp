import React, { useContext } from "react";
import "../styles/ConnectWallet.css";
import CoinbaseWalletLogo from "../images/coinbase.png";
import MetaMaskLogo from "../images/metamask-icon.png";
import LedgerLogo from "../images/ledger.png";
import WalletConnectLogo from "../images/walletconnect-circle-blue.png";
import { useWeb3React } from "@web3-react/core";
import { Injected, CoinbaseWallet, WalletConnect, Ledger } from "../Connectors";
import { toastContext } from "../App";

function ConnectWalletModal(props) {
  const { activate, error, deactivate } = useWeb3React();
  const { errorToast, successToast } = useContext(toastContext);

  // Metamask Connect
  async function metaMaskConnect() {
    if (window.ethereum && window.ethereum.isMetaMask) {
      if (window.ethereum.providers !== undefined) {
        window.ethereum.setSelectedProvider(window.ethereum.providers.find(({ isMetaMask }) => isMetaMask));
      }
      await activate(Injected);
    } else {
      errorToast("Please make sure you are connected to Sepolia Network with MetaMask!");
      console.log(window.ethereum.isMetaMask);
      console.log(window.ethereum.chainId);
    }
  }

  return (
    <div className="connect-wallet-modal">
      <div className="modal-bg" onClick={() => props.setClicked(false)}></div>
      <div className="container">
        {/* MetaMask */}
        <div className="metamask modal-wallet-item" onClick={metaMaskConnect}>
          <img src={MetaMaskLogo} />
          <h2>MetaMask</h2>
        </div>
        {/* Coinbase Wallet */}
        <div className="coinbase-wallet modal-wallet-item" onClick={() => activate(CoinbaseWallet)}>
          <img src={CoinbaseWalletLogo} />
          <h2>Coinbase Wallet</h2>
        </div>
        {/* WalletConnect */}
        <div className="wallet-connect modal-wallet-item" onClick={() => activate(WalletConnect)}>
          <img src={WalletConnectLogo} />
          <h2>WalletConnect</h2>
        </div>
        {/* Ledger */}
        <div className="torus modal-wallet-item" onClick={() => activate(Ledger)}>
          <img src={LedgerLogo} />
          <h2>Ledger</h2>
        </div>
      </div>
    </div>
  );
}

export default ConnectWalletModal;
