import { InjectedConnector } from "@web3-react/injected-connector";
import { WalletLinkConnector } from "@web3-react/walletlink-connector";
import { WalletConnectConnector } from "@web3-react/walletconnect-connector";
import { LedgerConnector } from "@web3-react/ledger-connector";

// Injected Wallet
export const Injected = new InjectedConnector({
  supportedChainIds: [11155111],
});

// CoinBase Wallet
export const CoinbaseWallet = new WalletLinkConnector({
  url: `https://sepolia.infura.io/v3/${process.env.REACT_APP_INFURA_KEY}`,
  appName: "Lottery",
  supportedChainIds: [11155111],
});

// Wallet Connect
export const WalletConnect = new WalletConnectConnector({
  rpcUrl: `https://sepolia.infura.io/v3/${process.env.INFURA_KEY}`,
  bridge: "https://bridge.walletconnect.org",
  qrcode: true,
});

// Ledger
export const Ledger = new LedgerConnector({ chainId: 11155111, url: `https://sepolia.infura.io/v3/${process.env.REACT_APP_INFURA_KEY}` });
