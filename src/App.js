import React, { createContext, useEffect } from "react";
import "./styles/App.css";
import ConnectWallet from "./components/ConnectWallet";
import { useWeb3React } from "@web3-react/core";
import Navbar from "./components/Navbar";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Lottery from "./components/Lottery";

export const toastContext = createContext();

function App() {
  const { active, chainId, account, error, connector, activate, deactivate, library } = useWeb3React();

  // Error Handling
  useEffect(() => {
    console.error(error);
    if (error !== undefined) {
      deactivate();
      // Unsupported Chain Id
      if (error.message.includes("Unsupported chain id")) {
        errorToast("Make sure to check you have selected Rinkeby Network.");
      } else {
        errorToast(error.message);
      }
    }
  }, [error]);

  // On Active
  useEffect(() => {
    if (active) {
      successToast("Wallet Connected");
    } else {
      deactivate();
    }
  }, [active]);

  // Error Toast
  function errorToast(str) {
    toast.error(str, {
      position: "bottom-center",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  }

  // Success Toast
  function successToast(str) {
    toast.success(str, {
      position: "bottom-center",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  }

  return (
    <toastContext.Provider value={{ errorToast, successToast }}>
      <div className="app">
        <div className="main">
          <Navbar />
          {active ? <Lottery /> : <ConnectWallet />}
        </div>
        <ToastContainer />
      </div>
    </toastContext.Provider>
  );
}

export default App;
