# Lottery Dapp Made With React
Website - https://evildeedz.gitlab.io/lottery-dapp/
Sepolia Smart Contract - https://sepolia.etherscan.io/address/0xDE7107bcf94c2BC9ec7515B7781C5521d54211Ad

<img src="https://i.imgur.com/1bz6Pj0.png">

## About This Project
This is a premade Smart Contract that I deployed on the Sepolia Network. It is a very simple Smart Contract, thus doesn't have roles setup. This makes the "Pick Winner" function only callable by the deployer of the Smart Contract. However, anyone can add money to the pool.   

## How to Test the Pick Winner Function
If anyone wants it they can ask me on discord (AL. EviL DeEdZ#1809), or fork the project. The smart contract is in the repo as well. 

**Note:** Some of the wallet need RPC url to connect. Save your infura RPC key to an .env variable in the project root folder as 
`REACT_APP_INFURA_KEY` 
